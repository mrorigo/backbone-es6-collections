import Backbone from 'backbone';

// Based on https://github.com/p3drosola/Backbone.VirtualCollection
// Available under the MIT License (MIT);

let sortedIndexTwo = function(array, obj, iterator, context) {
    "use strict";
    let low = 0, high = array.length;
    while (low < high) {
        let mid = (low + high) >> 1;
        if(iterator.call(context, obj, array[mid]) > 0) {
            low = mid + 1;
        }
        else {
            high = mid;
        }
    }
    return low;
};


export default class BackboneVirtualCollection extends Backbone.Collection {

    constructor(collection, options) {
        this.collection = collection;
        this.options = options;

        if (options.comparator !== undefined) {
            this.comparator = options.comparator;
        }
        if (options.close_with) {
            this.bindLifecycle(options.close_with, 'close'); // Marionette 1.*
        }
        if (options.destroy_with) {
            this.bindLifecycle(options.destroy_with, 'destroy'); // Marionette 2.*
        }
        if (collection.model) {
            this.model = collection.model;
        }

        this.accepts = this.buildFilter(options.filter);
        this._rebuildIndex();
        this.listenTo(this.collection, 'add', this._onAdd);
        this.listenTo(this.collection, 'remove', this._onRemove);
        this.listenTo(this.collection, 'change', this._onChange);
        this.listenTo(this.collection, 'reset',  this._onReset);
        this.listenTo(this.collection, 'sort',  this._onSort);
        this._proxyParentEvents(['sync', 'request', 'error']);

        this.initialize.apply(this, arguments);
    }

    // Marionette 1.*
    bindLifecycle (view, method_name) {
        view.on(method_name, _.bind(this.stopListening, this));
    }

    updateFilter(filter) {
        this.accepts = this.buildFilter(filter);
        this._rebuildIndex();
        this.trigger('filter', this, filter);
        this.trigger('reset', this, filter);
        return this;
    }

    _rebuildIndex() {
        _.invoke(this.models, 'off', 'all', this._onAllEvent, this);
        this._reset();
        this.collection.each((model, i) => {
            if (this.accepts(model, i)) {
                model.on('all', this._onAllEvent, this);
                this.models.push(model);
                this._byId[model.cid] = model;
                if (model.id) {
                    this._byId[model.id] = model;
                }
            }
        });

        this.length = this.models.length;

        if (this.comparator) {
            this.sort({silent: true});
        }
    }

    orderViaParent (options) {
        this.models = this.collection.filter((model) => {
            return (this._byId[model.cid] !== undefined);
        });
        if (!options.silent) {
            this.trigger('sort', this, options);
        }
    }

    _onSort(collection, options) {
        if (this.comparator !== undefined) {
            return;
        }
        this.orderViaParent(options);
    }

    _proxyParentEvents(events) {
        _.each(events, (eventName) => {
            this.listenTo(this.collection, eventName, _.partial(this.trigger, eventName));
        });
    }

    _onAdd(model, collection, options) {
        let already_here = this.get(model);
        if (!already_here && this.accepts(model, options.index)) {
            this._indexAdd(model);
            model.on('all', this._onAllEvent, this);
            this.trigger('add', model, this, options);
        }
    }
    
    _onRemove(model, collection, options) {
        if (!this.get(model)) {
            return;
        }

        let i = this._indexRemove(model);
        let options_clone = _.clone(options);
        options_clone.index = i;
        model.off('all', this._onAllEvent, this);
        this.trigger('remove', model, this, options_clone);
    }

    _onChange(model, options) {
        if (!model || !options) {
            return; // ignore malformed arguments coming from custom events
        }
        let already_here = this.get(model);

        if (this.accepts(model, options.index)) {
            if (already_here) {
                if (!this._byId[model.id] && model.id) {
                    this._byId[model.id] = model;
                }
                this.trigger('change', model, this, options);
            } else {
                this._indexAdd(model);
                this.trigger('add', model, this, options);
            }
        } else {
            if (already_here) {
                let i = this._indexRemove(model);
                let options_clone = _.clone(options);
                options_clone.index = i;
                this.trigger('remove', model, this, options_clone);
            }
        }
    }

    _onReset(collection, options) {
        this._rebuildIndex();
        this.trigger('reset', this, options);
    }

    sortedIndex(model, value, context) {
        let iterator = _.isFunction(value) ? value : function(target) {
            return target.get(value);
        };

        if (iterator.length === 1) {
            return _.sortedIndex(this.models, model, iterator, context);
        } else {
            return sortedIndexTwo(this.models, model, iterator, context);
        }
    }

    _indexAdd(model) {
        if (this.get(model)) {
            return;
        }
        let i;
        // uses a binsearch to find the right index
        if (this.comparator) {
            i = this.sortedIndex(model, this.comparator, this);
        } else if (this.comparator === undefined) {
            i = this.sortedIndex(model, (target) => {
                //TODO: indexOf traverses the array every time the iterator is called
                return this.collection.indexOf(target);
            });
        } else {
            i = this.length;
        }
        this.models.splice(i, 0, model);
        this._byId[model.cid] = model;
        if (model.id) {
            this._byId[model.id] = model;
        }
        this.length += 1;
    }

    _indexRemove(model) {
        model.off('all', this._onAllEvent, this);
        let i = this.indexOf(model);
        if (i === -1) {
            return i;
        }
        this.models.splice(i, 1);
        delete this._byId[model.cid];
        if (model.id) {
            delete this._byId[model.id];
        }
        this.length -= 1;
        return i;
    }
    
    _onAllEvent(eventName) {
        let explicitlyHandledEvents = ['add', 'remove', 'change', 'reset', 'sort'];
        if (!_.contains(explicitlyHandledEvents, eventName)) {
            this.trigger.apply(this, arguments);
        }
    }
    
    clone() {
        return new this.collection.constructor(this.models);
    }
    
    
  /* static */buildFilter(options) {
      if (!options) {
          return function () {
              return true;
          };
      } else if (_.isFunction(options)) {
          return options;
      } else if (options.constructor === Object) {
          return function (model) {
              return _.every(_.keys(options), function (key) {
                  return model.get(key) === options[key];
              });
          };
      }
  }
    add(...rest) {
        return this.collection.add.apply(this, ...rest);
    }
    
    remove(...rest) {
        return this.collection.remove.apply(this, ...rest);
    }
    
    set(...rest) {
        return this.collection.set.apply(this, ...rest);
    }
    
    reset(...rest) {
        return this.collection.reset.apply(this, ...rest);
    }
    
    push(...rest) {
        return this.collection.push.apply(this, ...rest);
    }
    
    pop(...rest) {
        return this.collection.pop.apply(this, ...rest);
    }
    
    unshift(...rest) {
        return this.collection.unshift.apply(this, ...rest);
    }
    
    shift(...rest) {
        return this.collection.shift.apply(this, ...rest);
    }
    
    slice(...rest) {
        return this.collection.slice.apply(this, ...rest);
    }
    
    sync(...rest) {
        return this.collection.sync.apply(this, ...rest);
    }
    
    fetch(...rest) {
        return this.collection.fetch.apply(this, ...rest);
    }
    
    url(...rest) {
        return this.collection.url.apply(this, ...rest);
    }
}

