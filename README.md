# Specialized Backbone Collections

I was missing ES6 versions of these classes, so I refactored them. Enjoy!

## GroupedCollection

Based on https://github.com/p3drosola/Backbone.GroupedCollection


## VirtualCollection

Based on https://github.com/p3drosola/Backbone.VirtualCollection